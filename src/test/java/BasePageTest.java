import constants.AuthenticationPageConstants;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import io.qameta.allure.Step;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AuthenticationPage;

import java.util.concurrent.TimeUnit;

import static constants.AuthenticationPageConstants.*;

public class BasePageTest {

    protected static WebDriver driver;

    @BeforeAll
    public static void setUp() {
        ChromeOptions options = new ChromeOptions().setHeadless(true);
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @AfterEach
    public void cleanUp(){
        driver.manage().deleteAllCookies();
    }

    @AfterAll
    public static void quit(){
        driver.quit();
    }

}

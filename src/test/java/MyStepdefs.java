import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pages.AuthenticationPage;
import pages.MyAccountPage;

import static constants.AuthenticationPageConstants.AUTHENTICATION_URL;
import static constants.MyAccountPageConstants.MY_ACCOUNT_PAGE_URL;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyStepdefs extends BasePageTest{

    private AuthenticationPage page;

    @Before
    public void initialization(){
        setUp();
    }

    @After
    public void teardown(Scenario scenario){
        if (scenario.isFailed()) {
//            attachScreenshot();
        }
        quit();
    }

//    @Attachment(value = "Failed test screenshot")
//    public byte[] attachScreenshot() {
//        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//    }

    @Given("I am on authentication page")
    public void iAmOnAuthenticationPage() {
        driver.get(AUTHENTICATION_URL);
        page = new AuthenticationPage(driver);
    }

    @When("I sign in with email {string} and password {string}")
    public void iSignInWithEmailAndPassword(String email, String password) {
        page.typeEmail(email);
        page.typePassword(password);
        page.clickSignInButton();
    }

    @Then("I should logged and be on my account page, username should be {string}")
    public void iShouldLoggedAndBeOnMyAccountPageUsernameShouldBe(String userName) {
        MyAccountPage myAccountPage = new MyAccountPage(driver);
        assertTrueCurrentUrlWithExpectedUrl(MY_ACCOUNT_PAGE_URL);
        assertEquals(myAccountPage.getTextUserNameLinkToMyAccount(), userName);
    }

    @Then("I shouldn't be logged")
    public void iShouldnTBeLogged() {
        assertTrueCurrentUrlWithExpectedUrl(AUTHENTICATION_URL);
    }

    @And("I should get error {string}")
    public void iShouldGetError(String errorMessage) {
        assertCurrentErrorMessageWithExpectedErrorMessage(errorMessage);
    }

    private void assertTrueCurrentUrlWithExpectedUrl(String expectedUrl) {
        String actualUrl = driver.getCurrentUrl();
        assertEquals(expectedUrl, actualUrl);
    }

    private void assertCurrentErrorMessageWithExpectedErrorMessage(String expectedErrorMessage) {
        String actualErrorMessage = page.getErrorMessage();
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Step(value = "Type email {0}")
    public void typeEmail(String email) {
        page.typeEmail(email);
    }

    @Step(value = "Type password {0}")
    public void typePassword(String password) {
        page.typePassword(password);
    }

    @Step(value = "Click button Login")
    public void clickSignInButton() {
        page.clickSignInButton();
    }
}

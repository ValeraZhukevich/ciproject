Feature: I want to test authentication page

  Scenario: I want to sign in with existing email and existing password
    Given I am on authentication page
    When I sign in with email "oydamn@fexpost.com" and password "qwerty"
    Then I should logged and be on my account page, username should be "Chack Norris"


  Scenario Outline: I want to sign in with not appropriate data
    Given I am on authentication page
    When I sign in with email "<email>" and password "<password>"
    Then I shouldn't be logged
    And I should get error "<message>"
    Examples:
      | email                  | password  | message                    |
      | oydamn@fexpost.com     | qwe997fsc | Authentication failed.     |
      | oydamn2000@fexpost.com | qwerty    | Authentication failed.     |
      | oydamn2000@fexpost.com | csdfe45   | Authentication failed.     |
      | oydamn@fexpost.com     | a123df    | Authentication failed.     |
      |                        |           | An email address required. |
      | oydamn@fexpost.com     |           | Password is required.      |
      |                        | qwerty    | An email address required. |
      | oydamnfexpost.com      | qwerty    | Invalid email address.     |
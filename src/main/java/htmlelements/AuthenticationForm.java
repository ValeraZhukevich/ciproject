package htmlelements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.AuthenticationPage;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

import static constants.AuthenticationPageConstants.EMAIL_FIELD;
import static constants.AuthenticationPageConstants.PASSWORD_FIELD;

public class AuthenticationForm extends HtmlElement {

    @Name("email")
    @FindBy(id = "email")
    private TextInput emailField;

    @Name("password")
    @FindBy(id = "passwd")
    private TextInput passwordField;

    @Name("Sign in button")
    @FindBy(id = "SubmitLogin")
    private Button buttonSignIn;


    public void clickSignInButton() {
        buttonSignIn.click();
    }

    public void typeEmail(String email) {
        emailField.sendKeys(email);
    }


    public void typePassword(String password) {
        passwordField.sendKeys(password);
    }

}

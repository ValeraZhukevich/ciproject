package constants;

import org.openqa.selenium.By;

public class MyAccountPageConstants {

    public static final By USER_NAME_LINK_TO_MY_ACCOUNT = By.xpath("//a[@class='account']/span");
    public static final String MY_ACCOUNT_PAGE_URL = "http://automationpractice.com/index.php?controller=my-account";
}

package constants;

import org.openqa.selenium.By;

public final class AuthenticationPageConstants {
    public static final By PASSWORD_FIELD = By.id("passwd");
    public static final By SIGN_IN_BUTTON = By.id("SubmitLogin");
    public static final By EMAIL_FIELD = By.id("email");
    public static final By ERROR_LI = By.xpath("//div[@class = 'alert alert-danger']/ol/li");

    public static final String AUTHENTICATION_URL = "http://automationpractice.com/index.php?controller=authentication";
}

package pages;

import org.openqa.selenium.WebDriver;
import static constants.MyAccountPageConstants.*;

public class MyAccountPage extends BasePage{

    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    public String getTextUserNameLinkToMyAccount(){
        return driver.findElement(USER_NAME_LINK_TO_MY_ACCOUNT).getText().trim();
    }

}

package pages;

import htmlelements.AuthenticationForm;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

public class AuthenticationPageWithPageFactory extends BasePage{

    @FindBy(id = "login_form")
    private AuthenticationForm authenticationForm;

    @FindBy(xpath = "//div[@class = 'alert alert-danger']/ol/li")
    private WebElement errorLi;


    public AuthenticationPageWithPageFactory(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new HtmlElementDecorator( new HtmlElementLocatorFactory(driver)), this);
    }

    public AuthenticationPageWithPageFactory typeEmail(String email){
        authenticationForm.typeEmail(email);
        return this;
    }


    public AuthenticationPageWithPageFactory typePassword(String password){
        authenticationForm.typePassword(password);
        return this;
    }

    public void clickSignInButton(){
        authenticationForm.clickSignInButton();
    }

    public String getErrorMessage(){
        return errorLi.getText().trim();
    }

}

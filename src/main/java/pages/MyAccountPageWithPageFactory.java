package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPageWithPageFactory extends BasePage{

    @FindBy(xpath = "//a[@class='account']/span")
    private WebElement userNameLinkToMyAccount;

    public MyAccountPageWithPageFactory(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTextUserNameLinkToMyAccount(){
        return userNameLinkToMyAccount.getText().trim();
    }

}

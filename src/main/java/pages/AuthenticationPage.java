package pages;

import htmlelements.AuthenticationForm;
import jdk.jfr.Name;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import static constants.AuthenticationPageConstants.*;

public class AuthenticationPage extends BasePage {

    @FindBy(id = "login_form")
    private AuthenticationForm authenticationForm;

    public AuthenticationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new HtmlElementDecorator( new HtmlElementLocatorFactory(driver)), this);
    }

    public AuthenticationPage typeEmail(String email){
        authenticationForm.typeEmail(email);
        return this;
    }


    public AuthenticationPage typePassword(String password){
        authenticationForm.typePassword(password);
        return this;
    }

    public void clickSignInButton(){
        authenticationForm.clickSignInButton();
    }

    public String getErrorMessage(){
        return driver.findElement(ERROR_LI).getText().trim();
    }

}
